#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2018 María Andrea Vignau mavignau@gmail.com

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA
# This code was inspired by:
#   http:#www.arduino.cc/en/Tutorial/Tone  by Tom Igoe


from notes import *

# notes in the melody:
# note durations: 4 = quarter note, 8 = eighth note, etc.:
melody = [(NOTE_C4, 4),
          (NOTE_G3, 8),
          (NOTE_G3, 8),
          (NOTE_A3, 4),
          (NOTE_G3, 4),
          (0,4),
          (NOTE_B3,4),
          (NOTE_C4,4)]

import rodi
import time
r = rodi.RoDI()

def triunfal():
  # iterate over the notes of the melody:
  for thisNote in melody:

    # to calculate the note duration, take one second divided by the note type.
    #e.g. quarter note = 1000 / 4, eighth note = 1000/8, etc.
    noteDuration = 1000 / thisNote[1]
    r.sing(thisNote[0], noteDuration)

    # to distinguish the notes, set a minimum time between them.
    # the note's duration + 30% seems to work well:
    pauseBetweenNotes = noteDuration * 1.30
    time.sleep(pauseBetweenNotes/1000)


def main():
  r.pixel(0,0,0)
  time.sleep(0.5)
  for i in range(40):
    if r.light() > 5:
      r.pixel(255,255,255)
      triunfal()
      break
    else:
      time.sleep(.2)
  #r.pixel(0, 0, 0)
main()
r.pixel(0, 0, 0)