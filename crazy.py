#!/usr/bin/env python
# -*- coding: utf-8 -*-
# Copyright (C) 2018 María Andrea Vignau mavignau@gmail.com

# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.

# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.

# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301
# USA

'''
RoDI (Robot Didactico Inalambrico) module
'''



import rodi
import time
import random

r = rodi.RoDI()

def boxes():
    "Follow a simple box-like circuit."
    #r.run_test()
    r.led(1)
    for i in [1,2,3,4]:
        r.move(100,100)
        r.pixel(0, 255, 0)
        time.sleep(2)

        r.move_left()
        r.pixel(255,0,0)
        time.sleep(.60)
        #print(r.imu())

    r.move_stop()
    r.led(0)
    r.pixel(0, 0, 256)

def flees():
    "Run straight until it see something, then it turns left. Change led colors from green to red."
    r.led(1)
    for i in range(20):
        if r.see() < 10:
            r.move_left()
            r.pixel(255, 0, 0)
        else:
            r.move_forward()
            r.pixel(0, 255, 0)
        time.sleep(0.3)

def sensors():
    """When put over dark floor, it turns leds red and go straight and slow.
    When over lighter floor, randomly turn left, right or go straith searching dark floor.
    """
    MAX_STOP = 3
    stop = MAX_STOP
    for i in range(120):
        n = sum(r.sense())
        if r.see() < 10:
            r.move_left()
            r.pixel(125, 0, 125)
        else:
            if n > 70:
                if stop == 0:
                    r.move(60, 60)
                    stop = MAX_STOP
                else:
                    r.move_stop()
                    r.sing(33, 1000)
                    stop -= 1
                    r.pixel(255, 0, 0)
            else:
                dir = random.randint(0,2)
                if dir == 0:
                    r.move(100,10)
                    r.pixel(0, 255, 0)
                elif dir == 2:
                    r.move(10,100)
                    r.pixel(0, 0, 255)
                else:
                    r.move(100,100)
                    r.pixel(125, 0, 125)

        time.sleep(0.6)
    r.blink(0)

def end_show():
    """Turns off everything. Rests 2 seconds"""
    r.move_stop()
    r.pixel(0,0,0)
    r.led(0)
    time.sleep(2)

if __name__ == "__main__":
    opt= 100
    options = [(u"Box: sigue un trayecto fijo",boxes),
               (u"Flees: sigue derecho, esquiva obstáculos", flees),
               (u"Sensors: Busca el piso oscuro. Esquiva obstáculos",sensors)]
    msj = [u"Bienvenido! \n"] + \
          ["%d- %s"% (i+1,x[0]) for i, x in enumerate(options)]

    try:
        while opt:
            print('\n'.join(msj))
            opt = raw_input("Ingrese la rutina que quiere observar, 0 para salir:")
            try:
                opt = int(opt)
            except Exception,err:
                print(err)
                opt = 100
            finally:
                if opt > len(options):
                    print(u"Opción no válida")
                elif opt > 0:
                    func = options[int(opt)-1][1]
                    func()
                    print(u"Fin de la rutina")
                    end_show()
    finally:
        end_show()